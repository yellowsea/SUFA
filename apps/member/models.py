from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from rest_framework.exceptions import APIException


class MemberExists(APIException):
    default_code = 400
    default_detail = ...


class MemberManager(BaseUserManager):
    def create(self, student_id, name, gender, mobile, campus, password, favorite_club=None):
        if Member.objects.filter(id=student_id).exists():
            raise MemberExists
        else:
            member = self.model(
                id=student_id,
                name=name,
                gender=gender,
                mobile=mobile,
                campus=campus
            )
            if favorite_club:
                member.favorite_club = favorite_club
            member.set_password(password)
            member.save()
            return member


class Member(AbstractBaseUser):
    """
    社团成员
    """

    id = models.CharField(max_length=8, primary_key=True, verbose_name='学号')
    name = models.CharField(max_length=50, verbose_name='姓名')
    gender = models.CharField(max_length=6,
                              choices=(
                                  ('male', '男'),
                                  ('female', '女')
                              ), verbose_name='性别')
    mobile = models.CharField(max_length=11, unique=True, verbose_name='电话')
    campus = models.CharField(max_length=2,
                              choices=(
                                  ('BS', '宝山'),
                                  ('YC', '延长'),
                                  ('JD', '嘉定')
                              ), verbose_name='校区')
    favorite_club = models.CharField(max_length=20, null=True, verbose_name='喜爱的球队')
    # 参加社团及校级及以上比赛需上传证件照认真
    photo = models.ImageField(upload_to='member/', max_length=100, null=True, blank=True, verbose_name='证件照')
    create_at = models.DateField(auto_now_add=True, verbose_name='加入社团时间')

    # 社团骨干
    is_admin = models.BooleanField(default=False, verbose_name='是否社团骨干')
    # 手机激活
    is_active = models.BooleanField(default=False, verbose_name='是否手机激活')
    # 学生证认证
    # 每学期第二周周一 0: 00 统一设为False, 需认证激活
    is_auth = models.BooleanField(default=True, verbose_name='是否学生证认证')

    objects = MemberManager()

    REQUIRED_FIELDS = [id, name, gender, mobile, campus]
    USERNAME_FIELD = 'id'

    class Meta:
        db_table = 'member'
        verbose_name = '社团成员'
        verbose_name_plural = verbose_name

    def get_username(self):
        return getattr(self, 'id')


class MemberClass(models.Model):
    """
    社团成员课程时间
    """

    member = models.OneToOneField(Member, on_delete=models.CASCADE, primary_key=True, verbose_name='社团成员')
    monday = models.PositiveSmallIntegerField(default=0, verbose_name='周一课程')
    tuesday = models.PositiveSmallIntegerField(default=0, verbose_name='周二课程')
    wednesday = models.PositiveSmallIntegerField(default=0, verbose_name='周三课程')
    thursday = models.PositiveSmallIntegerField(default=0, verbose_name='周四课程')
    friday = models.PositiveSmallIntegerField(default=0, verbose_name='周五课程')

    class Meta:
        db_table = 'member_class'
        verbose_name = '社团成员课程时间'
        verbose_name_plural = verbose_name


class Permission(models.Model):
    """
    社团管理平台权限
    """

    id = models.AutoField(auto_created=True, primary_key=True, verbose_name='编号')
    name = models.CharField(max_length=20, verbose_name='名称')
    description = models.CharField(max_length=100, verbose_name='描述')

    class Meta:
        db_table = 'permission'
        verbose_name = '权限'
        verbose_name_plural = verbose_name


class Department(models.Model):
    """
    部门
    """

    id = models.AutoField(auto_created=True, primary_key=True, verbose_name='编号')
    name = models.CharField(max_length=10, verbose_name='名称')
    description = models.CharField(max_length=200, verbose_name='简介')
    status = models.BooleanField(default=True, verbose_name='状态')
    permissions = models.ManyToManyField(Permission, through='DepartmentPermission', verbose_name='部门权限')

    class Meta:
        db_table = 'department'
        verbose_name = '部门'
        verbose_name_plural = verbose_name


class Position(models.Model):
    """
    职位
    """

    id = models.AutoField(auto_created=True, primary_key=True, verbose_name='编号')
    name = models.CharField(max_length=10, verbose_name='名称')
    remind = models.CharField(max_length=200, verbose_name='提醒事项')
    appointment = models.ForeignKey(Permission, on_delete=models.CASCADE, related_name='appointment_permission',
                                    verbose_name='任命职位所需权限')
    permissions = models.ManyToManyField(Permission, through='PositionPermission', related_name='position_permission',
                                         verbose_name='职位权限')

    class Meta:
        db_table = 'position'
        verbose_name = '职位'
        verbose_name_plural = verbose_name


class DepartmentPermission(models.Model):
    """
    部门权限
    """

    id = models.AutoField(auto_created=True, primary_key=True, verbose_name='编号')
    permission = models.ForeignKey(Permission, on_delete=models.CASCADE, verbose_name='权限')
    department = models.ForeignKey(Department, on_delete=models.CASCADE, verbose_name='部门')

    class Meta:
        db_table = 'department_permission'
        verbose_name = '部门权限'
        verbose_name_plural = verbose_name


class PositionPermission(models.Model):
    """
    职位权限
    """

    id = models.AutoField(auto_created=True, primary_key=True, verbose_name='编号')
    permission = models.ForeignKey(Permission, on_delete=models.CASCADE, verbose_name='权限')
    position = models.ForeignKey(Position, on_delete=models.CASCADE, verbose_name='职位')

    class Meta:
        db_table = 'position_permission'
        verbose_name = '职位权限'
        verbose_name_plural = verbose_name


class AdministratorApply(models.Model):
    """
    社团骨干职位申请
    """

    id = models.AutoField(auto_created=True, primary_key=True, verbose_name='编号')
    member = models.ForeignKey(Member, on_delete=models.CASCADE, verbose_name='社团成员')
    position = models.ForeignKey(Position, on_delete=models.CASCADE, verbose_name='申请职位')
    introduction = models.CharField(max_length=200, verbose_name='自我介绍')
    # -1: 审核未通过
    #  0: 待审核
    #  1: 审核通过
    status = models.SmallIntegerField(default=0, verbose_name='状态')

    class Meta:
        db_table = 'administrator_apply'
        verbose_name = '社团骨干申请'
        verbose_name_plural = verbose_name


class Administrator(models.Model):
    """
    社团骨干
    """

    member = models.OneToOneField(Member, on_delete=models.CASCADE, primary_key=True, verbose_name='社团成员')
    position = models.ForeignKey(Position, on_delete=models.CASCADE, verbose_name='职位')
    status = models.BooleanField(default=True, verbose_name='状态')

    class Meta:
        db_table = 'administrator'
        verbose_name = '社团骨干'
        verbose_name_plural = verbose_name
